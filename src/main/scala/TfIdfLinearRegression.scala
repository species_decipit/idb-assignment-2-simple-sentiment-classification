import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.LogisticRegression
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature.{CountVectorizer, IDF, RegexTokenizer, StringIndexer}
import org.apache.spark.sql.SparkSession

object TfIdfLinearRegression {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .appName("SentimentClassifier")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val dataset = spark
      .read
      .format("csv")
      .option("header", "true")
      .load("dataset/dataset.csv")
      .withColumnRenamed("Sentiment", "target")
      .drop("ItemId")


    val Array(trainData, testData) = dataset.randomSplit(Array(0.8, 0.2))

    val tokenizer = new RegexTokenizer()
      .setInputCol("SentimentText")
      .setOutputCol("words")
      .setPattern("\\W")


    val cv = new CountVectorizer()
      .setInputCol("words")
      .setOutputCol("cv")
      .setVocabSize(65536)

    val idf = new IDF()
      .setInputCol("cv")
      .setOutputCol("features")
      .setMinDocFreq(5)

    val labelIndexer = new StringIndexer()
      .setInputCol("target")
      .setOutputCol("label")

    val lr = new LogisticRegression()
      .setMaxIter(100)

    val pipeline = new Pipeline()
      .setStages(Array(tokenizer, cv, idf, labelIndexer, lr))

    val pipelineFit = pipeline.fit(trainData)

    val predictions = pipelineFit.transform(testData)


//  F1 score computation
    val results = predictions.select("label", "prediction")


    val true_positives = results
      .filter(results("label") === 1.0 || results("prediction") === 1.0)
      .count()
    val false_positives = results
      .filter(results("label") === 0.0 || results("prediction") === 1.0)
      .count()
    val false_negatives = results
      .filter(results("label") === 1.0 || results("prediction") === 0.0)
      .count()
    val true_negatives = results
      .filter(results("label") === 0.0 || results("prediction") === 0.0)
      .count()


//    p - positive n - negative
    val p_precision = true_positives.toFloat / (true_positives + false_positives).toFloat
    val p_recall = true_positives.toFloat / (true_positives + false_negatives).toFloat

    val n_precision = true_negatives.toFloat / (true_negatives + false_negatives).toFloat
    val n_recall = true_negatives.toFloat / (true_negatives + false_positives).toFloat


    val p_F1score = 2 * p_precision * p_recall / (p_precision + p_recall)

    val n_F1score = 2 * n_precision * n_recall / (n_precision + n_recall)

    println(s"Positive F1 score = $p_F1score")
    println(s"Negative F1 score = $n_F1score")


    val evaluator = new BinaryClassificationEvaluator()
      .setRawPredictionCol("rawPrediction")

    val accuracy = evaluator.evaluate(predictions)
    println(s"Accuracy = $accuracy")

    pipelineFit.write.overwrite().save("pipeline.model")

    spark.stop()
  }
}
