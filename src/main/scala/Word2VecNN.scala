import org.apache.spark.ml.Pipeline
import org.apache.spark.ml.classification.MultilayerPerceptronClassifier
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator
import org.apache.spark.ml.feature._
import org.apache.spark.sql.SparkSession


object Word2VecNN {
  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .appName("SentimentClassifier")
      .getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")

    val dataset = spark
      .read
      .format("csv")
      .option("header", "true")
      .load("dataset/dataset.csv")
      .withColumnRenamed("Sentiment", "target")
      .drop("ItemId")

    val tokenizer = new RegexTokenizer()
      .setInputCol("SentimentText")
      .setOutputCol("words")
      .setPattern("\\W")

    val word2Vec = new Word2Vec()
      .setInputCol("words")
      .setOutputCol("features")
      .setVectorSize(100)
      .setMinCount(0)

    val labelIndexer = new StringIndexer()
      .setInputCol("target")
      .setOutputCol("label")

    val Array(trainData, testData) = dataset.randomSplit(Array(0.8, 0.2))

    val layers = Array[Int](100, 75, 10, 2)
    val classifier = new MultilayerPerceptronClassifier()
      .setLayers(layers)
      .setBlockSize(128)
      .setSeed(1234L)
      .setMaxIter(100)


    val pipeline = new Pipeline()
      .setStages(Array(tokenizer, word2Vec, labelIndexer, classifier))

    val pipelineFit = pipeline.fit(trainData)

    val predictions = pipelineFit.transform(testData)


    //  F1 score computation
    val results = predictions.select("label", "prediction")


    val true_positives = results
      .filter(results("label") === 1.0 || results("prediction") === 1.0)
      .count()
    val false_positives = results
      .filter(results("label") === 0.0 || results("prediction") === 1.0)
      .count()
    val false_negatives = results
      .filter(results("label") === 1.0 || results("prediction") === 0.0)
      .count()
    val true_negatives = results
      .filter(results("label") === 0.0 || results("prediction") === 0.0)
      .count()


    //    p - positive n - negative
    val p_precision = true_positives.toFloat / (true_positives + false_positives).toFloat
    val p_recall = true_positives.toFloat / (true_positives + false_negatives).toFloat

    val n_precision = true_negatives.toFloat / (true_negatives + false_negatives).toFloat
    val n_recall = true_negatives.toFloat / (true_negatives + false_positives).toFloat


    val p_F1score = 2 * p_precision * p_recall / (p_precision + p_recall)

    val n_F1score = 2 * n_precision * n_recall / (n_precision + n_recall)

    println(s"Positive F1 score = $p_F1score")
    println(s"Negative F1 score = $n_F1score")


    val evaluator = new BinaryClassificationEvaluator()
      .setRawPredictionCol("rawPrediction")

    val accuracy = evaluator.evaluate(predictions)
    println(s"Accuracy = $accuracy")

    spark.stop()
  }
}
